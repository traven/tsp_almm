﻿using System;
using System.Collections.Generic;
using Core.FS.Model.Instance;

namespace Core.FS.Model.Instance
{
    public class FSInstance
    {
        public FSInstance(MachineString machines, List<BatchOfElements> batches, Dictionary<Tuple<Machine, BatchOfElements>, decimal> processingTime)
        {
            Machines = machines;
            Batches = batches;
            ProcessingTime = processingTime;

            CheckForInputDataErrors();
        }

        public MachineString Machines { get; private set; }
        public List<BatchOfElements> Batches { get; private set; }
        public Dictionary<Tuple<Machine, BatchOfElements>, decimal> ProcessingTime;

        private void CheckForInputDataErrors()
        {
            foreach (Machine machine in Machines.Machines)
                foreach (BatchOfElements batch in Batches)
                    if (!ProcessingTime.ContainsKey(new Tuple<Machine, BatchOfElements>(machine, batch)))
                        throw new MissingFieldException("Processing time undefined for some machines and batches");
        }
    }
}
