﻿using System;
using Core.ALMM.Model;

namespace Core.FS.Model.Instance
{
    public sealed class ElementType : Entity<int>
    {
        public string Name { get; private set; }

        public ElementType(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}