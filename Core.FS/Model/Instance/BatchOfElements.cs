﻿using System;
using System.Collections.Generic;
using Core.FS.Model.Instance;
using Core.ALMM.Model;

namespace Core.FS.Model.Instance
{
    public sealed class BatchOfElements : Entity<Guid>
    {
        public uint NumberOfElements { get; private set; }
        public decimal Deadline { get; private set; }
        public ElementType Type { get; private set; }

        public BatchOfElements(ElementType type, uint numberOfElements, decimal deadline)
        {
            Id = Guid.NewGuid();
            Type = type;
            NumberOfElements = numberOfElements;
            Deadline = deadline;
        }

        public BatchOfElements Divide(uint elementsToNewBatch)
        {
            if (elementsToNewBatch > NumberOfElements)
                throw new ArgumentOutOfRangeException("Cannot create new batch with more elements!");
            this.NumberOfElements -= elementsToNewBatch;
            return new BatchOfElements(this.Type, elementsToNewBatch, this.Deadline);
        }
    }
}
