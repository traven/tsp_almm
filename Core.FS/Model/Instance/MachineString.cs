﻿using System;
using System.Collections.Generic;
using Core.ALMM.Model;
using Core.FS.Model.Instance;

namespace Core.FS.Model.Instance
{
    public sealed class MachineString : Entity<Guid>
    {
        public List<Machine> Machines { get; private set; }
        public string Name { get; private set; }

        public MachineString(List<Machine> machines, string name)
        {
            Id = Guid.NewGuid();
            Name = name;
            Machines = machines;
        }
    }
}
