﻿using System;
using Core.ALMM.Model;

namespace Core.FS.Model.Instance
{
    public sealed class Machine : Entity<int>
    {
        public string Name { get; private set; }
        public Machine(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}
