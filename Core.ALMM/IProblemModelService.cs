﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.ALMM.Model.State;

namespace Core.ALMM
{
    /// <summary>
    /// reprezentuje konktretny problem tj. stan poczatkowy, funkcja przejscia oraz sprawdza czy dany stan
    /// jest stanem koncowym czy nie jest stanem koncowym
    /// </summary>
    public interface IProblemModelService
    {
        SystemState_S InitialState { get; set; }

        SystemState_S TransientFunction();

        bool IsUnacceptableStateSet(SystemState_S state);

        bool IsAcceptableStateSet(SystemState_S state);
    }
}
