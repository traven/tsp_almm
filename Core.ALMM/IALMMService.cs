﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.ALMM
{
    public interface IALMMService
    {
        void Process(string filename);
    }
}
