﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.ALMM.Model
{
    /// <summary>
    /// encja reprezentuje obiekt rozróznialny poprzez ID, kazdy obiekt posiadający takie samo ID jest taki sam
    /// 
    /// </summary>
    /// <typeparam name="T">Typ klucza encji</typeparam>
    public abstract class Entity<T>
    {
        public T Id { get; protected set; }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            Type type = obj.GetType();

            if (this.GetType() != type)
                return false;

            Entity<T> item = (Entity<T>) obj;
            return this.Id.Equals(item.Id);
        }

        public static bool operator ==(Entity<T> left, Entity<T> right )
        {
            if(Object.Equals(left, null))
                return Object.Equals(right, null);

            return left.Equals(right);
        }

        public static bool operator !=(Entity<T> left, Entity<T> right )
        {
            return !(left == right);
        }
    }
}
