﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.ALMM.Model.Decision
{
    /// <summary>
    /// reprezentuje jedna decyzje, jedna decyzja zawiera liste decyzji, poniewaz kazdy np. komiwojazer 
    /// podejmuje swoja decyzje, ktora jest inna dla wszystkich
    /// </summary>
    /// <typeparam name="TDecision"></typeparam>
    public class StateDecisionItem<TDecision> : Entity<Guid>, ICloneable
    {
        public StateDecisionItem()
        {
            DecisionEntries = new List<TDecision>();
        }

        public List<TDecision> DecisionEntries { get; private set; }

        public object Clone()
        {
            List<TDecision> decisionEntries = new List<TDecision>( this.DecisionEntries );

            StateDecisionItem<TDecision> sdi = new StateDecisionItem<TDecision>();
            sdi.DecisionEntries = decisionEntries;

            return sdi;
        }
    }
}
