﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.ALMM.Model.State
{
    /// <summary>
    /// Stan systemu w czasie t - Time
    /// Lista przechowuje liste stanów poszczególnych elementów w tym czasie
    /// </summary>
    public class SystemState_S : Entity<int>
    {
        public SystemState_S()
        {
            AppropriateState_x = new List<Entity<int>>();
        }

        public List<Entity<int>> AppropriateState_x { get; set; }

        public decimal Time { get; set; }
    }
}
