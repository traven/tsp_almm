﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.ALMM.Model.Decision;
using Core.ALMM.Model.State;

namespace Core.ALMM.Model.Result
{
    /// <summary>
    /// reprezentuje wezel trajektorii, tzn stan systemu w czasie T, liste mozliwych decyzji, 
    /// decyzję, która zostala wybrana w danym momencie czasowym
    /// </summary>
    /// <typeparam name="TDecision"></typeparam>
    public class NodeItem<TDecision> : Entity<int>
    {
        public NodeItem(SystemState_S state)
        {
            State = state;
        }

        public NodeItem()
        {
            State = new SystemState_S();
            Decisions = new List<StateDecisionItem<TDecision>>();
        }

        public SystemState_S State { get; set; }

        public List<StateDecisionItem<TDecision>> Decisions { get; set; }

        public StateDecisionItem<TDecision> SelectedDecision { get; set; }
    }
}
