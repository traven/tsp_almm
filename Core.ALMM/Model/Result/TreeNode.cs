﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.ALMM.Model.Result
{
    /// <summary>
    /// reprezentuje trajektorie wynikowa
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class TreeNode<T>
    {
        public List<TreeNode<T>> Children { get; private set; }

        public T Item { get; private set; }

        public TreeNode<T> Parent { get; private set; }

        public TreeNode(T item)
        {
            Item = item;
            Children = new List<TreeNode<T>>();
        }

        public TreeNode<T> AddChild(T item)
        {
            TreeNode<T> nodeItem = new TreeNode<T>(item);
            nodeItem.Parent = this;

            Children.Add(nodeItem);
            return nodeItem;
        }
    }
}
