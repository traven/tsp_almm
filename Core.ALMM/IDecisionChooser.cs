﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.ALMM.Model.Decision;

namespace Core.ALMM
{
    /// <summary>
    /// zaimplementowany interfejs powinien posiadac metode wybierajaca jedna decyzje z listy decyzji mozliwych w danym stanie
    /// </summary>
    /// <typeparam name="TDecision"></typeparam>
    public interface IDecisionChooser<TDecision>
    {
        StateDecisionItem<TDecision> SelectDecision(List<StateDecisionItem<TDecision>> decisionItems);
    }
}
