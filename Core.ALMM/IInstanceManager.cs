﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.ALMM
{
    /// <summary>
    /// reprezentuje instancje danego problemu
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IInstanceManager<T>
    {
        T CurrentInstance { get; set; }

        /// <summary>
        /// wczytuje plik xml z instancja problemu
        /// </summary>
        /// <param name="filename"></param>
        void LoadInstance(string filename);
    }
}
