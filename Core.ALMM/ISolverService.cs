﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.ALMM
{
    /// <summary>
    /// reprezentuje faktyczny solver, ktory powinen wykonywac poszczegolne etapy algorytmu almm, przy 
    /// wykorzystaniu innych klas
    /// wszystkie obiekty powinny byc przekazywane do tej klasy przez interfejs w konstruktorze
    /// </summary>
    public interface ISolverService
    {
        void ReadData(string filename);

        void SetStartState();

        void Generate_Up();

        void SelectDecision_u();

        void EvalNextState();

        bool IsUnacceptableState();

        void SaveUnacceptableState();

        bool IsFinalState();

        void SaveAcceptableState();
    }
}
