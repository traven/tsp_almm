﻿using Core.ALMM.Model.Result;

namespace Core.ALMM
{
    /// <summary>
    /// Klasa reprezentuje wynik dzialania systemu czyli trajektorie wynikowa oraz ostatnio doday element,
    /// obiekt powinien byc wykorzystany do budowania xml wyjsciowego
    /// </summary>
    /// <typeparam name="TDecision"></typeparam>
    public interface IALMMResultManager<TDecision>
    {
        TreeNode<NodeItem<TDecision>> TreeRoot { get; }

        TreeNode<NodeItem<TDecision>> LastAddedNodeState { get; }

        void AddState(NodeItem<TDecision> node);
    }
}
