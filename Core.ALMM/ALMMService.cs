﻿
namespace Core.ALMM
{
    /// <summary>
    /// Klasa reprezentuje algorytm almm, uruchomienie algorytmu, polega na wywolaniu funkcji process z nazwa pliku,
    /// z którego maja zostac pobrane dane
    /// 
    /// Klasa przyjmuje w konstruktorze obiekt solver service, który posiada zaimplementowane wszystkie metody,
    /// potrzebne do wykonania symulacji
    /// </summary>
    public class ALMMService : IALMMService
    {
        public readonly ISolverService _solverService;

        public ALMMService(ISolverService solverService)
        {
            _solverService = solverService;
        }

        public void Process(string filename)
        {
            _solverService.ReadData(filename);

            _solverService.SetStartState();

            do
            {
                _solverService.Generate_Up();

                _solverService.SelectDecision_u();

                _solverService.EvalNextState();

                if(_solverService.IsUnacceptableState())
                {
                    _solverService.SaveUnacceptableState();
                    break;
                }

                if( _solverService.IsFinalState() )
                {
                    _solverService.SaveAcceptableState();
                    break;
                }
            } while (true);
        }
    }
}
