﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Core.ALMM;
using Core.TSP.Model.Instance;

namespace Core.TSP
{
    public class TSPInstanceManager : IInstanceManager<TSPInstance>
    {
        public TSPInstance CurrentInstance { get; set; }

        public void LoadInstance(string filename)
        {
            int salesmanNumber;
            var salesmans = CurrentInstance.Salesmans;
            var cities = CurrentInstance.cities;
            var distances = CurrentInstance.distances;
            Console.WriteLine("this is reading file");
            XDocument linqDoc = XDocument.Load(filename);

            // wczytanie z xml
            var data = (from item in linqDoc.Descendants("salesman")
                        select item.Element("salesmanNumber").Value);

            salesmanNumber = Convert.ToInt32(data.First());
            data = from item in linqDoc.Descendants("salesman")
                   select item.Element("salesmanNumber").Value;

            int id;
            var citydata = from item in linqDoc.Descendants("city")
                           select new
                           {
                               cityid = item.Attribute("id").Value,
                               hqid = item.Element("headquarter").Value
                           };
            string cityMap;
            var citytab = from item in linqDoc.Descendants("cities")
                          select item.Element("cityMap").Value;

            // konwersja wczytanych danych
            for (int i = 0; i < salesmanNumber; i++)
            {
                salesmans.Add(new Salesman(i));     // dodaje sprzedawce o danym id (do sprawdzenia na potem czy dziala)
            }
            cityMap = citytab.First();

            //test tekstu
            Console.WriteLine("Found values");
            Console.WriteLine("Number of salesmans: {0}", salesmanNumber);
            foreach (var p in citydata)             // do sprawdzenia (!), wczytywanie danych o miastach
            {
                if (p.cityid == p.hqid)
                {
                    cities.Add(new Headquarters(Convert.ToInt32(p.cityid)));
                }
                if (p.cityid != p.hqid)
                {
                    cities.Add(new Branch(Convert.ToInt32(p.cityid), new Headquarters(Convert.ToInt32(p.hqid))));
                }
                // Console.WriteLine(p);   
            }
            Console.WriteLine("Map of city: {0}", cityMap);

            Console.WriteLine("END OF VALUES");
            Console.WriteLine("CreateMatrix");
            distances = new DistanceMatrix(cityMap);
        }
    }
}
