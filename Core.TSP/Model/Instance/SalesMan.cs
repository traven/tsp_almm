﻿using System;
using Core.ALMM.Model;

namespace Core.TSP.Model.Instance
{
    public sealed class Salesman : Entity<int>
    {
        private int id { get; set; }
        public Salesman(int id)      // tworzy salesmana o danym id
        {
            this.Id = id;
        }
    }
}
