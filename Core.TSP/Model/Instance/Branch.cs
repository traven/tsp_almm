﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.TSP.Model.Instance
{
    public class Branch : City
    {
        private Headquarters parent;
        public Branch(int id, Headquarters parent)
            : base(id)
        {
            this.parent = parent;
        }
    }
}
