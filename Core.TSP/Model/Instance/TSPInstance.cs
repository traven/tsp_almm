﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.TSP.Model.Instance
{
    public class TSPInstance
    {
        public TSPInstance(City startCity, List<Salesman> salesMans, List<City> cities, DistanceMatrix citiesGraph)
        {
            StartCity = startCity;
            Salesmans = salesMans;
            distances = citiesGraph;
            this.cities = cities;
        }

        public City StartCity { get; private set; }

        public List<Salesman> Salesmans { get; private set; }

        public DistanceMatrix distances { get; private set; }

        public List<City> cities { get; private set; }
    }
}
