﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.ALMM;
using Core.TSP.DecisionChooser;
using Core.TSP.Model.Instance;

namespace Core.TSP.Model
{
    class TestClass
    {
        static void Main(string[] args)
        {
            char key;
            IInstanceManager<TSPInstance> instanceManager = new TSPInstanceManager();
            IProblemModelService problemModelService = new TSPProblemModelService(instanceManager);
            IALMMResultManager<Decision.Decision> resultManager = new TSPResultManager();
            IDecisionChooser<Decision.Decision> decisionChooser = new RandomDecisionChooser();
            
            ISolverService solverService = new TSPSolverService( instanceManager, problemModelService, resultManager, decisionChooser );
            solverService.ReadData("C:\\Users\\lenovo\\Desktop\\C#\\solver32013\\TSP\\SalesmanProblem\\SalesmanProblem\\salesman.xml");
            /*
            //tworzenie przykładowego XML
            XDocument docToSave = new XDocument(
                new XDeclaration("1.0", "utf-8", "yes"),
                new XComment("Starbuzz Customer Loyalty Data"),
                new XElement("starbuzzData",
                    new XAttribute("storeName", "Park Slope"),
                    new XAttribute("location", "Brooklyn, NY"),
                    new XElement("person",
                        new XElement("personalInfo",
                            new XElement("name", "Janet Venutian"),
                            new XElement("zip", 11215)),
                        new XElement("favoriteDrink", "Choco Macchiato"),
                        new XElement("moneySpent", 255),
                        new XElement("visits", 50)),
                    new XElement("person",
                        new XElement("personalInfo",
                            new XElement("name", "Liz Nelson"),
                            new XElement("zip", 11238)),
                        new XElement("favoriteDrink", "Double Cappuccino"),
                        new XElement("moneySpent", 150),
                        new XElement("visits", 35)),
                    new XElement("person",
                        new XElement("personalInfo",
                            new XElement("name", "Matt Franks"),
                            new XElement("zip", 11217)),
                        new XElement("favoriteDrink", "Zesty Lemon Chai"),
                        new XElement("moneySpent", 75),
                        new XElement("visits", 15)),
                    new XElement("person",
                        new XElement("personalInfo",
                            new XElement("name", "Joe Ng"),
                            new XElement("zip", 11217)),
                        new XElement("favoriteDrink", "Banana Split in a Cup"),
                        new XElement("moneySpent", 60),
                        new XElement("visits", 10)),
                    new XElement("person",
                        new XElement("personalInfo",
                            new XElement("name", "Sarah Kalter"),
                            new XElement("zip", 11215)),
                        new XElement("favoriteDrink", "Boring Coffee"),
                        new XElement("moneySpent", 110),
                        new XElement("visits", 15))));
            docToSave.Save("C:\\Users\\lenovo\\Desktop\\C#\\test1_new.xml");
            */


         /*   //=====================================wbudowane=========================================
            XmlTextReader read = new XmlTextReader("C:\\Users\\lenovo\\Desktop\\C#\\solver32013\\TSP\\SalesmanProblem\\SalesmanProblem\\salesman.xml"); //jakiś wbudowany standard
            while (read.Read())
            {
                switch (read.NodeType)
                {
                    case XmlNodeType.Element: // The node is an element.
                        Console.Write("<" + read.Name);
                        Console.WriteLine(">");
                        break;
                    case XmlNodeType.Text: //Display the text in each element.
                        Console.WriteLine(read.Value);
                        break;
                    case XmlNodeType.EndElement: //Display the end of the element.
                        Console.Write("</" + read.Name);
                        Console.WriteLine(">");
                        break;
                }

            }
             
            */
            //==============================================LINQ===============================================
           while (true)
            {

                Console.WriteLine("Press q to exit");
                key = char.ToLower(Console.ReadKey().KeyChar);
                if (key.Equals('q')) break;
               // Console.Clear();
            }
        }
    }
}
