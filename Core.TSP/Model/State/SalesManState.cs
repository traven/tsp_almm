﻿using Core.ALMM.Model;
using Core.TSP.Model.Instance;

namespace Core.TSP.Model.State
{
    public class SalesManState : Entity<int>
    {
        public SalesManState(int id)
        {
            Id = id;
        }

        public City DestinationCity { get; set; }

        public decimal LengthOfRoad { get; set; }
    }
}
