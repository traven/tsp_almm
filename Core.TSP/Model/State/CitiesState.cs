﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.ALMM.Model;
using Core.TSP.Model.Instance;

namespace Core.TSP.Model.State
{
    public class CitiesState : Entity<int>
    {
        public CitiesState()
        {
            VisitedCity = new List<City>();
        }

        public List<City> VisitedCity { get; set; }
    }
}
