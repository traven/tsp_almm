﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.ALMM.Model;
using Core.TSP.Model.Instance;

namespace Core.TSP.Model.Decision
{
    public class Decision : Entity<int>
    {
        public Decision(int id)
        {
            Id = id;
        }

        public City City { get; set; }
    }
}
