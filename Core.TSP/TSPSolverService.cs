﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.ALMM;
using Core.ALMM.Model.Decision;
using Core.ALMM.Model.Result;
using Core.ALMM.Model.State;
using Core.TSP.Model.Decision;
using Core.TSP.Model.Instance;
using Core.TSP.Model.State;

namespace Core.TSP
{
    public class TSPSolverService : ISolverService
    {
        private readonly IInstanceManager<TSPInstance> _instanceManager;
        private readonly IProblemModelService _problemModelService;
        private readonly IALMMResultManager<Decision> _resultManager;
        private readonly IDecisionChooser<Decision> _decisionChooser;

        public TSPSolverService(IInstanceManager<TSPInstance> instanceManager, IProblemModelService problemModelService,
                    IALMMResultManager<Decision> resultManager, IDecisionChooser<Decision> decisionChooser )
        {
            _instanceManager = instanceManager;
            _problemModelService = problemModelService;
            _resultManager = resultManager;
            _decisionChooser = decisionChooser;
        }

        public void ReadData(string filename)
        {
            _instanceManager.LoadInstance(filename);
        }

        public void SetStartState()
        {
            SystemState_S state = new SystemState_S();

            state.AppropriateState_x.Add(new CitiesState());
            TSPInstance instance = _instanceManager.CurrentInstance;
            foreach (Salesman man in instance.Salesmans)
            {
                state.AppropriateState_x.Add(new SalesManState(man.Id) { DestinationCity = instance.StartCity, LengthOfRoad = 0 });
            }
            _resultManager.AddState(new NodeItem<Decision>(state ));
        }

        public void Generate_Up()
        {
            throw new NotImplementedException();
            //_resultManager.AddState( new NodeItem<Decision>() { Decisions = stateDecisions } );
        }

        public void SelectDecision_u()
        {
            throw new NotImplementedException();
        }

        public void EvalNextState()
        {
            throw new NotImplementedException();
        }

        public bool IsUnacceptableState()
        {
            return _problemModelService.IsUnacceptableStateSet(_resultManager.LastAddedNodeState.Item.State);
        }

        public void SaveUnacceptableState()
        {
            throw new NotImplementedException();
        }

        public bool IsFinalState()
        {
            return _problemModelService.IsAcceptableStateSet(_resultManager.LastAddedNodeState.Item.State);
        }

        public void SaveAcceptableState()
        {
            throw new NotImplementedException();
        }
    }
}
