﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.ALMM;
using Core.ALMM.Model.State;
using Core.TSP.Model.Instance;
using Core.TSP.Model.State;

namespace Core.TSP
{
    public class TSPProblemModelService : IProblemModelService
    {
        private IInstanceManager<TSPInstance> _instanceManager;

        public SystemState_S InitialState { get; set; }

        public TSPProblemModelService(IInstanceManager<TSPInstance> instanceManager)
        {
            _instanceManager = instanceManager;
        }

        public SystemState_S TransientFunction()
        {
            throw new NotImplementedException();
        }

        public bool IsUnacceptableStateSet(SystemState_S state)
        {
            throw new NotImplementedException();
        }

        public bool IsAcceptableStateSet(SystemState_S state)
        {
            throw new NotImplementedException();
        }
    }
}
