﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GraphSharp.Controls;
using QuickGraph;

namespace GUI
{
    public class LGraphLayout : GraphLayout<Vertex, Edge<Vertex>, BidirectionalGraph<Vertex,Edge<Vertex>>> { }
    class MainWindowViewModel 
    {
        public BidirectionalGraph<Vertex, Edge<Vertex>> Tree { get; set; }
        public string LayoutAlgorithmType { get; set; }

        public MainWindowViewModel()
        {
            Tree = new BidirectionalGraph<Vertex, Edge<Vertex>>(true);
            List<Vertex> vertices = new List<Vertex>();
            vertices.Add(new Vertex(1,"pierwszy"));
            vertices.Add(new Vertex(2,"drugi"));
            vertices.Add(new Vertex(3, "trzeci"));
            vertices.Add(new Vertex(4, "czwarty"));

            vertices.Add(new Vertex(5, "łostatni"));
            vertices.Add(new Vertex(7, "A taki zboczny"));
            foreach (Vertex v in vertices)
                Tree.AddVertex(v);
            Tree.AddEdge(new Edge<Vertex>(vertices[0],vertices[1]));
            Tree.AddEdge(new Edge<Vertex>(vertices[1], vertices[2]));
            Tree.AddEdge(new Edge<Vertex>(vertices[2], vertices[3]));
            Tree.AddEdge(new Edge<Vertex>(vertices[3], vertices[4]));
            Tree.AddEdge(new Edge<Vertex>(vertices[2], vertices[5]));
            LayoutAlgorithmType = "Tree";
        }
        

        
    }
}
