﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuickGraph;

namespace GUI
{
    /// <summary>
    /// Basic Vertex type.
    /// </summary>
    public class Vertex
    {
        public int Id { get; private set; }
        public string Content { get; private set; }

        public Vertex(int id, string content)
        {
            Id = id;
            Content = content;
        }
        public Vertex()
        {
            Id = -1;
            Content = "Bad initialisation. Use Vertex(id, content).";
        }
    }
}
